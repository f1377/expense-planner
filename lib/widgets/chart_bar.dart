import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPercentageOfTotal;

  ChartBar(
    this.label,
    this.spendingAmount,
    this.spendingPercentageOfTotal,
  );

  Column chartBarColumn(BuildContext context, BoxConstraints constraints) {
    final double maxAvailableHeight = constraints.maxHeight;

    final double amountHeight = maxAvailableHeight * 0.15;
    final double spaceHeight1 = maxAvailableHeight * 0.05;
    final double barHeight = maxAvailableHeight * 0.6;
    final double spaceHeight2 = maxAvailableHeight * 0.05;
    final double labelHeight = maxAvailableHeight  * 0.15;

    return Column(
      children: <Widget>[
        Container(
          height: amountHeight,
          child: FittedBox(
            child: Text('€${spendingAmount.toStringAsFixed(0)}'),
          ),
        ),
        SizedBox(
          height: spaceHeight1,
        ),
        Container(
          height: barHeight,
          width: 10,
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: 1.0,
                  ),
                  color: Color.fromRGBO(220, 220, 220, 1),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              FractionallySizedBox(
                heightFactor: spendingPercentageOfTotal,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: spaceHeight2,
        ),
        Container(
          height: labelHeight,
          child: FittedBox(child: Text(label))
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return chartBarColumn(context, constraints);
    },);
  }
}
